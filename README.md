# gobblerr

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.vscode-typescript-vue-plugin).

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```


### NOTES
While Vue's syntax is rather cumbersome and ugly, I found myself able to read the documentation a lot easier than React's. Time to completion of this task was 1.5 hours, and I wasn't struggling with CORS issues. 

yarn run dev --host to expose the server.

Templating was easy, GET and POST requests were a bit difficult especially with understanding how functions are called and inputs are mapped.

Build took 1.34 seconds to compile. 
Total size of dist package: 84KB